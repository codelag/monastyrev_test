module EmployeesHelper
  def tree(roots)
    tag.ul do
      roots.order(:name).each do |root|
        class_name = root.children.present? ? 'opened' : ''
        concat(tag.li root.name, class: class_name)
        if root.children.present?
          concat(tree_level(root.children, ''))
        end
      end
    end
  end

  def tree_level(nodes, ul_class)
    tag.ul class: ul_class do
      nodes.order(:name).each do |node|
        class_name = node.children.present? ? 'closed' : ''
        concat(tag.li node.name, class: class_name)
        if node.children.present?
          concat(tree_level(node.children, 'hidden'))
        end
      end
    end
  end
end
