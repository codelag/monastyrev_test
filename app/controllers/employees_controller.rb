class EmployeesController < ApplicationController
  def index
    @employees = Employee.all
  end

  def employee_select
    if params[:id].present?
      @roots = Employee.where(id: params[:id])
    else
      @roots = Employee.all.roots
    end

    respond_to do |format|
      format.js 
    end
  end
end
