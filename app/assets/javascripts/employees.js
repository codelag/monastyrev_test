$( document ).ready(function() {
  $('#employee_id').change(function() {
    $("#list").html('<div>Loading...</div>')

    function ajax_call(employee_id) {
      $.get('employees/employee_select', {id: employee_id}, null, 'script')
    }

    setTimeout(ajax_call(this.value), 500);
  });
});

$(document).on('click', '#list li.closed, #list li.opened', (function(){
  $(this).toggleClass("closed opened");
  $(this).next('ul').toggleClass('hidden')
}))