# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

15.times do |x|
  Employee.create(id: x+1, name: Faker::Name.name_with_middle)
end

Employee.first.children << Employee.second
Employee.second.children << Employee.third
Employee.third.children << Employee.fourth
first_four_ids = Employee.order(:id).limit(4).pluck(:id)
Employee.order(id: :desc).limit(11).each do |e|
  e.update_attributes(parent_id: first_four_ids.sample)
end